# Read README before launch project


## To correct start launch project from scene called `MenuScene`.

## TODO:

- [x] Refactor InputManger according to  OOP principles (add state machine for different types of input)
- [ ] Add background music
- [ ] Add some SFX for shooting, player moving, enemy death e.t.c.
- [ ] Fix bug with score counter
- [ ] Refactor according to  OOP principles (interfaces for enemys/bullets, abstract classes)
- [ ] Add text shadow
- [ ] Add UI for displaing lives
