using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveController : MonoBehaviour
{
    public float speed;
    public Transform container;
    
    void Update()
    {
        container.Translate(Vector3.down * speed * Time.deltaTime);
    }
}
