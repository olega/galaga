using UnityEngine;

[DisallowMultipleComponent]
public class PingPongMover : MonoBehaviour
{
    public Vector3 movePosition;
    [Range(0, 1)] public float moveProgress;
    public float moveSpeed;

    private Vector3 _startPosition;
    
    void Start()
    {
        _startPosition = transform.position;
    }

    void Update()
    {
        moveProgress = Mathf.PingPong(Time.time * moveSpeed, 1);
        Vector3 offset = movePosition * moveProgress;
        transform.position = _startPosition + offset; 
    }
}

