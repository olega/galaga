using UnityEngine;
using UnityEngine.UI;

public class EnemyController : MonoBehaviour
{
    public int health;
    public Animator animator;
    public int score;
    public Text scoreText;

    public void Update()
    {
        if (health <= 0)
        {
            animator.Play("EnemyDeath");
            Destroy(gameObject, 0.3f);
            Counter();
        }
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
    }

    public void Counter()
    {
        score++;
        scoreText.text = score.ToString();
    }
}
