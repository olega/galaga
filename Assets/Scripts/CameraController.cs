using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform player;
    public Transform cameraTransform;
    public float speed;

    public float leftLimit;
    public float rightLimit;
    public float upLimit;
    public float downLimit;

    private void Awake()
    {
        cameraTransform.position = new Vector3(
            player.position.x,
            player.position.y,
            player.position.z - 10);
    }

    void Update()
    {
        if (player)
        {
            Vector3 target = new Vector3(
                player.position.x,
                player.position.y,
                player.position.z - 10);

            Vector3 pos = Vector3.Lerp
            (cameraTransform.position,
                target,
                speed * Time.deltaTime);

            cameraTransform.position = pos;
        }        
        
        cameraTransform.position = new Vector3
            (
            Mathf.Clamp(transform.position.x, leftLimit, rightLimit),
            Mathf.Clamp(transform.position.y, upLimit, downLimit),
            transform.position.z
            );
    }
}
