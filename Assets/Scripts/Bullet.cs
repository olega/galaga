using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed;
    public float lifeTime;
    public int damage;
    public GameObject prefab;
    
    
    void Update()
    {
        transform.Translate(Vector2.up * speed * Time.deltaTime);
        Destroy(prefab, lifeTime);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other != null)
        {
            if (other.CompareTag("Enemy"))
            {
                other.GetComponent<EnemyController>().TakeDamage(damage);
                Destroy(prefab);
            }
            if (other.CompareTag("EnemyMid"))
            {
                other.GetComponent<EnemyMidController>().TakeDamage(damage);
                Destroy(prefab);
            }
            if (other.CompareTag("EnemyBoss"))
            {
                other.GetComponent<EnemyBossController>().TakeDamage(damage);
                Destroy(prefab);
            }
        }
    }
}
