using System;
using System.Collections;
using UnityEngine;

public class EnemyMidController : MonoBehaviour
{
    public int health;
    public Animator animator;
    public Transform bulletSpawnPoint;
    public GameObject bullet;
    public float startShotDelay;
    public float shotDelay;

    private void Start()
    {
        Invoke("StartShooting", startShotDelay);
    }

    public void Update()
    {
        if (health <= 0)
        {
            animator.Play("EnemyDeath");
            Destroy(gameObject, 0.4f);
        }
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
    }

    IEnumerator Shoot(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            Instantiate(bullet, bulletSpawnPoint.position, bulletSpawnPoint.rotation);
        }
    }

    private void StartShooting()
    {
        StartCoroutine(Shoot(shotDelay));
    }
}