using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public Animator animator;
    public Transform playerTransform;
    public int health;
    public GameObject gameOverText;
    public GameObject pressEscText;


    private BaseInputController _currentController;

    private void Start()
    {
        _currentController = new DefaultInputController(speed, animator, playerTransform);
    }

    void Update()
    {
        _currentController.UpdateControll();
        
        if (health <= 0)
        {
            animator.Play("Death");
            Destroy(gameObject, 0.4f);
        }

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Enemy"))
        {
            OnDeath();
        }
        if (other.CompareTag("EnemyMid"))
        {
            OnDeath();
        }
        if (other.CompareTag("BulletEnemy"))
        {
            OnDeath();
        }
    }
    
    public void TakeDamage(int damage)
    {
        health -= damage;
    }
    
    private void OnDeath()
    {
        _currentController = new BlockedInputController();
        animator.Play("Death");
        Destroy(gameObject, 0.4f);
        gameOverText.SetActive(true);
        pressEscText.SetActive(true);
    }
}
