using UnityEngine;

public class BulletEnemy : MonoBehaviour
{
    public float speed;
    public float lifeTime;
    public int damage;
    public GameObject prefab;
    
    
    void Update()
    {
        transform.Translate(Vector2.down * speed * Time.deltaTime);
        Destroy(gameObject, lifeTime);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other != null)
        {
            if (other.CompareTag("Player"))
            {
                other.GetComponent<PlayerController>().TakeDamage(damage);
                Destroy(prefab);
            }
        }
    }
}