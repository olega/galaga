using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    public float speed;
    
    public float end;
    public float start;

    private void Update()
    {
        transform.Translate(Vector2.down * speed * Time.deltaTime);

        if (transform.position.y <= end)
        {
            Vector2 pos = new Vector2(transform.position.x, start);
            transform.position = pos;
        }
    }
}
