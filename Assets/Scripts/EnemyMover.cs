using DG.Tweening;
using UnityEngine;

public class EnemyMover : MonoBehaviour
{
    public Transform moverTransform;
    public Vector3[] waypoints;
    public float duration;
    public PathType pathType;
    public PathMode pathMode;
    
    void Start()
    {
        moverTransform.DOPath(waypoints, duration, pathType, pathMode);
    }
}
