using UnityEngine;

public class DefaultInputController : BaseInputController
{
    private readonly float _speed;
    private readonly Animator _animator;
    private readonly Transform _playerTransform;

    public DefaultInputController(float speed, Animator animator, Transform playerTransform)
    {
        _speed = speed;
        _animator = animator;
        _playerTransform = playerTransform;
    }

    public override void UpdateControll()
    {
        if (Input.GetKey(KeyCode.A))
        {
            _playerTransform.Translate(Vector3.left * _speed * Time.deltaTime);
            _animator.Play("Left");
        }
        if (Input.GetKey(KeyCode.D))
        {
            _playerTransform.Translate(Vector3.right * _speed * Time.deltaTime);
            _animator.Play("Right");
        }
        if (Input.GetKey(KeyCode.S))
            _playerTransform.Translate(Vector3.down * _speed * Time.deltaTime);
        if (Input.GetKey(KeyCode.W))
            _playerTransform.Translate(Vector3.up * _speed * Time.deltaTime);
    }
}