using System.Collections;
using UnityEngine;

public class EnemyBossController : MonoBehaviour
{
    public int health;
    public Animator animator;
    public Transform bulletSpawnPoint1;
    public Transform bulletSpawnPoint2;
    public Transform bulletSpawnPoint3;

    public GameObject bullet;
    public float startShotDelay;
    public float shotDelay;

    private void Start()
    {
        Invoke("StartShooting", startShotDelay);
    }

    public void Update()
    {
        if (health <= 0)
        {
            animator.Play("EnemyDeath");
            Destroy(gameObject, 0.4f);
        }
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
    }

    IEnumerator Shoot(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            Instantiate(bullet, bulletSpawnPoint1.position, bulletSpawnPoint1.rotation);
            Instantiate(bullet, bulletSpawnPoint2.position, bulletSpawnPoint2.rotation);
            Instantiate(bullet, bulletSpawnPoint3.position, bulletSpawnPoint3.rotation);
        }
    }

    private void StartShooting()
    {
        StartCoroutine(Shoot(shotDelay));
    }
}