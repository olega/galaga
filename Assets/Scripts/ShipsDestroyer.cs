using UnityEngine;

public class ShipsDestroyer : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Enemy"))
        {
            Destroy(other.gameObject);
        }
        if (other.CompareTag("EnemyMid"))
        {
            Destroy(other.gameObject);
        }
    }
}
